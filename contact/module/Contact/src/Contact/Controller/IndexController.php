<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Contact\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Contact\Model\ContactQuery;
use Contact\Model\Contact;

class IndexController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function dashboardAction() {
        $collectionContact = ContactQuery::create()->find();
        return new ViewModel(array("collectionContact" => $collectionContact));
    }

    public function consulterAction() {
        $contactSelectionne = ContactQuery::create()->findOneById($this->params('id'));
        return new ViewModel(['unContact' => $contactSelectionne]);
    }

    public function modifierAction() {

        if ($this->getRequest()->isPost()) {
            $id = $this->getRequest()->getPost('id');
            $contactSelectionne = ContactQuery::create()->findOneById($id);

            $nom = $this->getRequest()->getPost('nom');
            $prenom = $this->getRequest()->getPost('prenom');
            $telephone = $this->getRequest()->getPost('telephone');
            $email = $this->getRequest()->getPost('email');

            $contactSelectionne->setNom($nom);
            $contactSelectionne->setPrenom($prenom);
            $contactSelectionne->setTelephone($telephone);
            $contactSelectionne->setEmail($email);

            $contactSelectionne->save();
            return $this->redirect()->toRoute('rContact-Gestion', array('action' => 'consulter', 'id' => $id));
        } else {
            $id = $this->params('id');
            $contactSelectionne = ContactQuery::create()->findOneById($id);
        }


        return new ViewModel(['unContact' => $contactSelectionne]);
    }

    public function ajouterAction() {
        if ($this->getRequest()->isPost()) {
            $contact = new Contact();

            $nom = $this->getRequest()->getPost('nom');
            $prenom = $this->getRequest()->getPost('prenom');
            $telephone = $this->getRequest()->getPost('telephone');
            $email = $this->getRequest()->getPost('email');

            $contact->setNom($nom);
            $contact->setPrenom($prenom);
            $contact->setTelephone($telephone);
            $contact->setEmail($email);

            $contact->save();
            return $this->redirect()->toRoute('rContact-Gestion', array('action' => 'dashboard'));
        }
    }

    public function supprimerAction() {
        $contactSelectionne = ContactQuery::create()->findOneById($this->params('id'));
        $contactSelectionne->delete();
        return $this->redirect()->toRoute('rContact-Gestion', array('action' => 'dashboard'));
    }


    public function rechercherAction() {
        $nom = $this->getRequest()->getPost('nom');
        $contact = ContactQuery::create()->filterByNom($nom)->findOne();
        return new ViewModel(['unContact' => $contact]);
    }

}
