<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Tableau de parametres
return array(
    //routage
    'router'=> array(
        //ensemble de routes
        'routes'=> array(
            //route nommé rContact
            'rContact'=> array(
                //type de route
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'=> '/',
                    'defaults' => array(
                        //Controleur par défaut
                        'controller' => 'CtrlIndex',
                        //Action par défaut
                        'action' => 'index',
                    ),
                ),
            ),
            'rContact-Gestion' => array(
              'type' => 'segment',
              'options' => array(
                'route' => '/:action[/:id][/:nom]',
                  'defaults' => array(
                    'controller' => 'CtrlIndex',
                     'constraints' => array(
                       'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                       'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                     ),
                  ),
              ),
            ),
        ),
    ),//FIN ROUTAGE
    //controller
    'controllers' => array(
        //controleur dynamiquement initialisable
        'invokables' => array(
            'CtrlIndex' => 'Contact\Controller\IndexController'
        ),
    ), //FIN CONTROLLER
    //Gestionnaire de vue
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'contact/index/index' => __DIR__ . '/../view/contact/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'DefaultNavigationFactory' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
    ),
    'navigation' => array(
        'default' => array(
            array(
              'label' => 'Accueil',
              'route' => 'rContact',
              'action' => 'index',
            ),
            array(
              'label' => 'Dashboard',
              'route' => 'rContact-Gestion',
              'action' => 'dashboard',
            ),
            array(
              'label' => 'Rechercher',
              'route' => 'rContact-Gestion',
              'action' => 'rechercher',
            ),
        ),
    ),
);//FIN PARAMETRES

